const app = new Vue({
  el: "#app",
  data: { // Dep.id = 2
    message: "Vue.js 源码分析",
    person: { // Dep.id = 4-5
      name: "doubleWJ"
    },
    list: ['SOS', '234', 'Hello', 'Mac'], // Dep.id = 7-8
    zero: 'This is a Component',
    coder: `<div class="point">
              <h2>灵活</h2>
              <p>不断繁荣的生态系统，可以在一个库和一套完整框架之间自如伸缩。</p>
            </div>`
  },
  mounted() {

  },
  methods: {

  }
});

console.log(app);


const docShow = () => {
  (function anonymous(
  ) {
    with (this) {
      return _c('div', { attrs: { "id": "app" } }, [
        _c('div', [_v("\n        Vue.js 框架源码与进阶\n      ")]),
        _v(" "),
        _m(0),
        _v(" "),
        _c('div', [_c('p', [_v(_s(message))])]),
        _v(" "),
        _c('div', [_c('p', [_v(_s(person.name))])]),
        _v(" "),
        _c('div', [_c('view-zoo', { attrs: { "zero": zero } })], 1),
        _v(" "),
        _c('div', { domProps: { "innerHTML": _s(coder) } })])
    }
  })
  console.log(app.$options.render.toString());
}

docShow();


